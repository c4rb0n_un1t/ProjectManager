#pragma once

#include <QtCore>
namespace {
QString getRepository(QString& parentDir)
{
	QProcess process;
	process.start("git", {"-C", parentDir, "remote", "get-url", "origin"});
	process.waitForFinished();
	return QString(process.readAllStandardOutput()).remove('\n');
}

bool hasLocalChanges(QString& parentDir)
{
	QProcess process;
	process.start("git", {"-C", parentDir, "status", "--porcelain"});
	process.waitForFinished();
	return !process.readAllStandardOutput().isEmpty();
}

bool isUpdateAvaliable(QString& parentDir)
{
	QProcess process;
	process.start("git", {"-C", parentDir, "fetch", "--dry-run"});
	process.waitForFinished();
	auto output = process.readAllStandardOutput();

	return !output.isEmpty();
}
}
