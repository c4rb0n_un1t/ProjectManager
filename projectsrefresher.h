#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Utility/i_dev_plugin_info_data_extention.h"
#include "../../Interfaces/Utility/i_dev_project_manager_data_extention.h"

class ProjectsRefresher : public QThread
{
	Q_OBJECT
public:
	ProjectsRefresher(ReferenceInstancePtr<IDevPluginInfoDataExtention>& pluginDevInfo);
	void setProjectPath(QFileInfo projectPath);

signals:
	void complete();

private:
	void run() override;
	QList<QFileInfo> searchForPlugins(QString directory);

private:
	QFileInfo m_projectPath;
	QMap<QString, int> m_pluginToId;
	ReferenceInstancePtr<IDevPluginInfoDataExtention>& m_pluginDevInfo;
	void addPlugin(QTextStream &stream, const ExtendableItemDataMap &data, const QPointer<IExtendableDataModel>& model);
	void updatePlugin(QTextStream& stream, int id, const ExtendableItemDataMap& data, const QPointer<IExtendableDataModel>& model);
};
