#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
{
	m_impl = new ProjectManager(this, m_pluginDevInfo, m_devProjectManager);
	initPluginBase(
	{
		{INTERFACE(IPlugin), this}
		, {INTERFACE(IProjectManager), m_impl}
	},
	{
		{INTERFACE(IDevPluginInfoDataExtention), m_pluginDevInfo},
		{INTERFACE(IDevProjectManagerDataExtention), m_devProjectManager},
	});
}

Plugin::~Plugin()
{
}

void Plugin::onReady()
{
	m_impl->onPluginReady();
}
