#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "projectmanager.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.ProjectManager" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	~Plugin() override;

private:
	ProjectManager* m_impl;
	ReferenceInstancePtr<IDevPluginInfoDataExtention> m_pluginDevInfo;
	ReferenceInstancePtr<IDevProjectManagerDataExtention> m_devProjectManager;

	// PluginBase interface
protected:
	void onReady() override;
};
