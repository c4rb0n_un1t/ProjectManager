#include "projectsrefresher.h"
#include "common.h"

ProjectsRefresher::ProjectsRefresher(ReferenceInstancePtr<IDevPluginInfoDataExtention>& pluginDevInfo) :
	m_pluginDevInfo(pluginDevInfo)
{
}

void ProjectsRefresher::setProjectPath(QFileInfo projectPath)
{
	m_projectPath = projectPath;
}

void ProjectsRefresher::addPlugin(QTextStream& stream, const ExtendableItemDataMap& data, const QPointer<IExtendableDataModel>& model)
{
	model->appendItem(data);
	auto&& pluginData = data[INTERFACE(IDevPluginInfoDataExtention)];
	if(!pluginData["isEnabled"].toBool())
		stream << '#';
	stream << pluginData["path"].toString() << " \\ #" << pluginData["repository"].toString() << Qt::endl;
}

void ProjectsRefresher::updatePlugin(QTextStream& stream, int id, const ExtendableItemDataMap& data, const QPointer<IExtendableDataModel>& model)
{
	model->updateItem(id, data);
	auto&& pluginData = data[INTERFACE(IDevPluginInfoDataExtention)];
	if(!pluginData["isEnabled"].toBool())
		stream << '#';
	stream << pluginData["path"].toString() << " \\ #" << pluginData["repository"].toString() << Qt::endl;
}

void ProjectsRefresher::run() {
	QMap<int, ExtendableItemDataMap> updatedData;
	QList<ExtendableItemDataMap> newData;
	auto model = m_pluginDevInfo->getModel();

	// Fill m_pluginToId with existing DB entries.
	auto ids = model->getItemIds();
	for(auto& id : ids)
	{
		auto item = model->getItem(id);
		const auto& data = item[INTERFACE(IDevPluginInfoDataExtention)];
		m_pluginToId[data["path"].toString()] = id;
	}
	qDebug() << "ids" << ids;

	// Save all existing plugin names to find which one is deprecated later.
	QStringList deprecatedPlugins = m_pluginToId.keys();

	// Fill newData and updatedData with project file entries.
	QFile file(m_projectPath.absoluteFilePath());
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QTextStream stream(&file);
		while (!stream.atEnd()) {
			QString line = stream.readLine();
			if(line.contains("TEMPLATE", Qt::CaseSensitive) || line.contains("SUBDIRS", Qt::CaseSensitive) ||
			        line.contains("MASS-Plugins/Resources", Qt::CaseSensitive))
				continue;

			bool isEnabled = line.at(0) != '#';

			QRegularExpression filenameRegex("^#?(.+?)\\s");
			auto&& filename = filenameRegex.match(line).captured(1);

			QRegularExpression nameRegex("[\\/]([\\w\\d]+).pro");
			auto&& name = nameRegex.match(filename).captured(1);

			auto&& parentDir = QFileInfo(filename).dir().path();
			auto repository = getRepository(parentDir);
			auto updateAvailable = isUpdateAvaliable(parentDir);
			auto hasLocalChanges_ = hasLocalChanges(parentDir);

			ExtendableItemDataMap&& data = {
				{	INTERFACE(IDevPluginInfoDataExtention), {
						{"name", name},
						{"path", filename},
						{"repository", repository},
						{"isEnabled", isEnabled},
						{"updateAvailable", updateAvailable},
						{"hasLocalChanges", hasLocalChanges_},
					}
				}
			};
			auto iter = m_pluginToId.find(filename);
			if(iter == m_pluginToId.end())
			{
				newData.append(data);
				qDebug() << "New data:" << data;
			}
			else
			{
				updatedData[*iter] = data;
				qDebug() << "Updated data:" << data;
			}
		}
		file.close();
	}

	// Fill newData with non-tracked plugin directories.
	auto filesInfo = searchForPlugins(m_projectPath.absolutePath());
	for(auto& file : filesInfo)
	{
		const auto& filename = file.absoluteFilePath();
		if(filename.contains("MASS-Plugins/Resources"))
			continue;
		if(m_pluginToId.contains(filename))
		{
			// If m_pluginToId contains same filename as found plugin, then remove it from deprecated list.
			deprecatedPlugins.removeOne(filename);
		}
		else
		{
			QRegularExpression nameRegex("[\\/]([\\w\\d]+).pro");
			auto&& name = nameRegex.match(filename).captured(1);

			auto&& parentDir = QFileInfo(filename).dir().path();
			auto repository = getRepository(parentDir);
			auto updateAvailable = isUpdateAvaliable(parentDir);
			auto hasLocalChanges_ = hasLocalChanges(parentDir);

			ExtendableItemDataMap&& data = {
				{	INTERFACE(IDevPluginInfoDataExtention), {
						{"name", name},
						{"path", filename},
						{"repository", repository},
						{"isEnabled", true},
						{"updateAvailable", updateAvailable},
						{"hasLocalChanges", hasLocalChanges_},
					}
				}
			};
			newData.append(data);
			qDebug() << "New data:" << data;
		}
	}

	// Remove deprecated plugins from model.
	// Only plugins that doesn't exist in file system but exist in DB will be removed.
	if(!deprecatedPlugins.isEmpty())
	{
		for(const auto& plugin : deprecatedPlugins)
		{
			auto id = m_pluginToId[plugin];
			model->removeItem(id);
		}
	}

	// Add new and update updated entries in DB.
	if(!newData.isEmpty() || !updatedData.isEmpty())
	{
		QFile file(m_projectPath.absoluteFilePath());
		if (file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
		{
			QTextStream stream(&file);
			stream << "TEMPLATE = subdirs" << Qt::endl << "SUBDIRS += \\" << Qt::endl;

			for(const auto& data : newData)
			{
				addPlugin(stream, data, model);
			}
			for(auto iter = updatedData.begin(); iter != updatedData.end(); ++iter)
			{
				updatePlugin(stream, iter.key(), iter.value(), model);
			}
			file.close();
		}
	}
	emit complete();
}

QList<QFileInfo> ProjectsRefresher::searchForPlugins(QString directory)
{
	QList<QFileInfo> filesInfo;
	QDirIterator it(directory, QDir::Dirs | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
	while(it.hasNext())
	{
		it.next();
		const auto& fileInfo = it.fileInfo();
		filesInfo.append(QDir(it.filePath()).entryInfoList({"*.pro"}, QDir::Files));
		searchForPlugins(fileInfo.absoluteFilePath());
	}
	return filesInfo;
}
