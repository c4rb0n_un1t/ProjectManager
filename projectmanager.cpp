#include "projectmanager.h"
#include "common.h"

ProjectManager::ProjectManager(QObject* parent, ReferenceInstancePtr<IDevPluginInfoDataExtention>& pluginDevInfo,
        ReferenceInstancePtr<IDevProjectManagerDataExtention>& devProjectManager) :
	QObject(parent),
	m_pluginDevInfo(pluginDevInfo),
	m_devProjectManager(devProjectManager)
{
	m_projectsRefresher.reset(new ProjectsRefresher(m_pluginDevInfo));
}

ProjectManager::~ProjectManager()
{
}

QString ProjectManager::projectFilePath()
{
	return m_projectPath.absoluteFilePath();
}

bool ProjectManager::setProjectFilePath(QString path)
{
	path.remove("file://");
	if(m_projectPath.absoluteFilePath() == path)
		return true;
	QFileInfo fileInfo(path);
	if(!fileInfo.isFile())
	{
		return false;
	}
	auto model = m_devProjectManager->getModel();
	auto index = model->index(0,0);
	auto parentDir = QFileInfo(path).dir().absolutePath();
	auto repository = getRepository(parentDir);
	if(index.isValid())
	{
		model->updateItem(index, {
			{	INTERFACE(IDevProjectManagerDataExtention), {
					{"path", path},
					{"repository", repository},
				}
			}
		});
	}
	else
	{
		model->appendItem({
			{	INTERFACE(IDevProjectManagerDataExtention), {
					{"path", path},
					{"repository", repository},
				}
			}
		});
	}
	m_projectPath = QFileInfo(path);
	m_projectsRefresher->setProjectPath(m_projectPath);
	emit projectFilePathChanged(projectFilePath());
	refreshExistingProjects();
	return true;
}

void ProjectManager::refreshExistingProjects()
{
	auto model = m_pluginDevInfo->getModel();
	m_projectsRefresher->start();
	connect(m_projectsRefresher.data(), &ProjectsRefresher::complete, this, &ProjectManager::onRefreshComplete);
}

void ProjectManager::downloadPluginFromRepository(QUrl url)
{
}

void ProjectManager::removePluginRepository(QString pluginPath)
{
	QDir(pluginPath).removeRecursively();
}

void ProjectManager::onPluginReady()
{
	auto projectModel = m_devProjectManager->getModel();
	auto index = projectModel->index(0,0);
	if(index.isValid())
	{
		const auto& item = projectModel->getFirstItem();
		auto filename = item[INTERFACE(IDevProjectManagerDataExtention)]["path"].toString();
		m_projectPath = QFileInfo(filename);
		m_projectsRefresher->setProjectPath(m_projectPath);
	}

	auto pluginModel = m_pluginDevInfo->getModel();
	connect(pluginModel, SIGNAL(itemUpdated(int, const ExtendableItemDataMap&, const ExtendableItemDataMap&)),
	        this, SLOT(onItemUpdated(int, const ExtendableItemDataMap&, const ExtendableItemDataMap&)));
}

void ProjectManager::onItemUpdated(int id, const ExtendableItemDataMap& before, const ExtendableItemDataMap& after)
{
	auto item = m_pluginDevInfo->getModel()->getItem(id);
	auto data = item[INTERFACE(IDevPluginInfoDataExtention)];
}

void ProjectManager::onRefreshComplete()
{
}
