#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Utility/iprojectmanager.h"
#include "../../Interfaces/Utility/i_dev_plugin_info_data_extention.h"
#include "../../Interfaces/Utility/i_dev_project_manager_data_extention.h"

#include "projectsrefresher.h"

class ProjectManager : public QObject, public IProjectManager
{
	Q_OBJECT
	Q_INTERFACES(IProjectManager)
	Q_PROPERTY(QString projectFilePath READ projectFilePath WRITE setProjectFilePath NOTIFY projectFilePathChanged)

public:
	ProjectManager(QObject* parent, ReferenceInstancePtr<IDevPluginInfoDataExtention>& pluginDevInfo,
	        ReferenceInstancePtr<IDevProjectManagerDataExtention>& devProjectManager);
	virtual ~ProjectManager() override;

	// IProjectManager interface
public slots:
	QString projectFilePath() override;
	bool setProjectFilePath(QString path) override;

public slots:
	void refreshExistingProjects() override;
	void downloadPluginFromRepository(QUrl url) override;
	void removePluginRepository(QString pluginPath) override;

public:
	void onPluginReady();

signals:
	void projectFilePathChanged(QString path) override;

private slots:
	void onItemUpdated(int id, const ExtendableItemDataMap& before, const ExtendableItemDataMap& after);
	void onRefreshComplete();

private:
	QSharedPointer<ProjectsRefresher> m_projectsRefresher;
	QFileInfo m_projectPath;
	QMap<QString, int> m_pluginToId;
	ReferenceInstancePtr<IDevPluginInfoDataExtention>& m_pluginDevInfo;
	ReferenceInstancePtr<IDevProjectManagerDataExtention>& m_devProjectManager;
};
